package activite2.specification1;

public abstract class Personnage {
    protected String nom;
    protected int pointsVie, x, y, v, precision; // Précision de 0 à 100
    protected Arme arme;  // Nouveau champ pour stocker l'arme du personnage

    public Personnage(String nom, int x, int y, int v, int precision) {
        this.nom = nom;
        this.x = x;
        this.y = y;
        this.pointsVie = 100;
        this.v = v;
        this.precision = precision;
        this.arme = null;  // Initialise l'arme à null par défaut
    }

    public int getPointsVie() {
        return this.pointsVie;
    }

    public void setPointsVie(int pointsVie) {
        this.pointsVie = pointsVie;
    }

    public int getPrecision() {
        return this.precision;
    }

    public void seDeplacer(int dx, int dy, int t) {
        this.x = (int) (this.x + dx * this.v * t / Math.sqrt(dx * dx + dy * dy));
        this.y = (int) (this.y + dy * this.v * t / Math.sqrt(dx * dx + dy * dy));
    }

    public void equiperArme(Arme arme) {
        this.arme = arme;
    }

    public void attaquer(Cible cible) {
        if (arme != null) {
            int puissanceAttaque = arme.getPuissance();
            cible.recevoirDegats(puissanceAttaque);
        } else {
            System.out.println("Le personnage n'a pas d'arme pour attaquer.");
        }
    }

    // Méthode abstraite pour définir le comportement de la cible lorsqu'elle est touchée
    public abstract void recevoirDegats(int degats);
}
