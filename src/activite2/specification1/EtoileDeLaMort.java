package activite2.specification1;

public class EtoileDeLaMort extends VaisseauPilote {

    private static EtoileDeLaMort instance;

    private EtoileDeLaMort(Personnage pilote) {
        super(1000, 5, pilote);
    }

    public static EtoileDeLaMort getInstance(Personnage pilote) {
        if (instance == null) {
            instance = new EtoileDeLaMort(pilote);
        }
        return instance;
    }

    @Override
    public boolean tirer(Cible cible) {
        // La précision de l'Étoile de la Mort est toujours de 100%
        return true;
    }
}
