package activite2.specification1;

public class PersonnageCible implements Cible {
    private int pointsVie;

    public PersonnageCible(int pointsVie) {
        this.pointsVie = pointsVie;
    }

    @Override
    public void recevoirDegats(int degats) {
        pointsVie -= degats;
        System.out.println("La cible a subi " + degats + " dégâts. Points de vie restants : " + pointsVie);
    }
}

