package activite2.specification1;

import java.util.Random;

public class VaisseauPilote extends VaisseauSpatial {
    private Personnage pilote;

    public VaisseauPilote(int puissance, int blindage, Personnage pilote) {
        super(puissance, blindage);
        this.pilote = pilote;
    }

    public Personnage getPilote() {
        return pilote;
    }

    @Override
    public void recevoirDegats(int degats) {
        // Réduction des dégâts si le vaisseau est piloté par un personnage
        int degatsReduits = degats / 2;
        super.recevoirDegats(degatsReduits);
    }

    public boolean tirer(Cible cible) {
        // Vérifie si le tir touche en fonction de la précision du pilote
        int randomValue = new Random().nextInt(100); // Génère un nombre aléatoire entre 0 et 99
        return randomValue < pilote.getPrecision();
    }
}
