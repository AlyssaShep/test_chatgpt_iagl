package activite2.specification1;

import java.util.Random;

public class Drone extends VaisseauSpatial {
    private int precisionIntrinseque;

    public Drone(int puissance, int blindage, int precisionIntrinseque) {
        super(puissance, blindage);
        this.precisionIntrinseque = precisionIntrinseque;
    }

    public int getPrecisionIntrinseque() {
        return precisionIntrinseque;
    }

    public boolean tirer(Cible cible) {
        // Vérifie si le tir touche en fonction de la précision intrinsèque du drone
        int randomValue = new Random().nextInt(100); // Génère un nombre aléatoire entre 0 et 99
        return randomValue < precisionIntrinseque;
    }
}
