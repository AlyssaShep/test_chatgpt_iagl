package activite2.specification1;

public class Jedi extends Personnage {
    public Jedi(String nom, int x, int y, int v, int precision) {
        super(nom, x, y, v, precision);
    }

    @Override
    public void recevoirDegats(int degats) {
        // Implémentation du comportement du Jedi lorsqu'il reçoit des dégâts
        // ...
    }

}