package activite2.specification1;

public class Arme {
    private int puissance;

    public Arme(int puissance) {
        this.puissance = puissance;
    }

    public int getPuissance() {
        return puissance;
    }
}