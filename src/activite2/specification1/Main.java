package activite2.specification1;

public class Main {
    public static void main(String[] args) {

        // a)
        // Création d'un personnage
        Personnage jedi = new Jedi("Luke Skywalker", 0, 0, 10, 80);
        // Création d'une arme
        Arme sabreLaser = new Arme(20);
        // Équipement du personnage avec l'arme
        jedi.equiperArme(sabreLaser);
        // Création d'une cible (autre personnage)
        Cible darkVador = new PersonnageCible(100);
        // Attaque de la cible par le personnage
        jedi.attaquer(darkVador);

        // b)
        VaisseauSpatial xwing = new VaisseauSpatial(30, 3);
        // Attaque du vaisseau spatial par une arme
        Arme laserCanon = new Arme(25);
        xwing.recevoirDegats(laserCanon.getPuissance());

        // c)
        // Création d'un personnage pilote
        Personnage pilote = new Jedi("Luke Skywalker", 0, 0, 10, 80);

        // Création d'un vaisseau piloté par le personnage
        VaisseauPilote xwingPilote = new VaisseauPilote(30, 3, pilote);

        // Création d'un drone
        Drone drone = new Drone(25, 2, 70);

        // Exemple de tir du vaisseau piloté
        if (xwingPilote.tirer(drone)) {
            System.out.println("Le tir du vaisseau piloté a touché le drone !");
        } else {
            System.out.println("Le tir du vaisseau piloté a manqué le drone.");
        }

        // Exemple de tir du drone
        if (drone.tirer(xwingPilote)) {
            System.out.println("Le tir du drone a touché le vaisseau piloté !");
        } else {
            System.out.println("Le tir du drone a manqué le vaisseau piloté.");
        }

        // d)
        // Création d'un personnage pilote
        Personnage piloteEtoileDeLaMort = new Jedi("Dark Vador", 0, 0, 10, 100);

        // Création de l'Étoile de la Mort
        EtoileDeLaMort etoileDeLaMort = EtoileDeLaMort.getInstance(piloteEtoileDeLaMort);

        // Création d'une cible
        Cible cible = new PersonnageCible(500);

        // Exemple de tir de l'Étoile de la Mort
        if (etoileDeLaMort.tirer(cible)) {
            System.out.println("L'Étoile de la Mort a détruit la cible !");
        } else {
            System.out.println("L'Étoile de la Mort a manqué la cible.");
        }

    }
}
