package activite2.specification1;

public class VaisseauSpatial extends Arme implements Cible {
    private int etat; // de 0 (détruit) à 100 (état neuf)
    private int blindage; // de 1 à 5

    public VaisseauSpatial(int puissance, int blindage) {
        super(puissance);
        this.etat = 100; // État initial neuf
        this.blindage = blindage;
    }

    public int getEtat() {
        return etat;
    }

    public int getBlindage() {
        return blindage;
    }

    @Override
    public void recevoirDegats(int degats) {
        int degatsSubis = degats / blindage;
        etat = Math.max(0, etat - degatsSubis); // L'état ne peut pas être négatif
        System.out.println("Le vaisseau spatial a subi " + degatsSubis + " dégâts. État restant : " + etat);
    }
}
