package activite2.specification2;

public abstract class Personnage {
    protected String nom;
    protected int pointsVie, connaissances, x, y, v;

    public Personnage(String nom, int x, int y, int v) {
        this.nom = nom;
        this.x = x;
        this.y = y;
        this.pointsVie = 100;
        this.connaissances = 0;
        this.v = v;
    }

    public int getPointsVie() {
        return this.pointsVie;
    }

    public void setPointsVie(int pointsVie) {
        this.pointsVie = pointsVie;
    }

    public int getConnaissances() {
        return this.connaissances;
    }

    public void setConnaissances(int connaissances) {
        this.connaissances = connaissances;
    }

    public void seDeplacer(int dx, int dy, int t) {
        // Implémentation de la méthode seDeplacer à compléter
        // ...
    }

    public abstract String parler();
}

