package activite2.specification2;

import java.util.ArrayList;

public abstract class Humanoid extends Personnage {
    protected ArrayList<Objet> objets;

    public Humanoid(String nom, int x, int y, int v) {
        super(nom, x, y, v);
        this.objets = new ArrayList<>();
    }

    public void acquérirObjet(Objet objet) {
        objets.add(objet);
        System.out.println(super.nom + " a acquis l'objet : " + objet.getNom());
    }

    public void seSéparerObjet(Objet objet, Humanoid destinataire) {
        if (objets.contains(objet)) {
            objets.remove(objet);
            destinataire.acquérirObjet(objet);
            System.out.println(super.nom + " a donné l'objet " + objet.getNom() + " à " + destinataire.nom);
        } else {
            System.out.println(super.nom + " ne possède pas l'objet " + objet.getNom());
        }
    }

    public void utiliserArme(String nomArme, Personnage cible) {
        for (Objet objet : objets) {
            if (objet instanceof Arme && objet.getNom().equals(nomArme)) {
                Arme arme = (Arme) objet;
                int degatsInfliges = arme.getPuissance();
                cible.setPointsVie(cible.getPointsVie() - degatsInfliges);
                System.out.println(super.nom + " utilise " + arme.getNom() + " contre " + cible.nom +
                        " et inflige " + degatsInfliges + " points de dégâts.");
                return;
            }
        }

        // Si l'arme n'est pas trouvée dans les objets du personnage
        System.out.println(super.nom + " ne possède pas l'arme " + nomArme + ".");
    }


    public void lireDocument(Document document) {
        document.lire(this);
    }
}
