package activite2.specification2;

public class Troll extends Personnage implements Monstre {
    private int puanteur;
    private int force;

    public Troll(String nom, int x, int y, int v, int force, int puanteur) {
        super(nom, x, y, v);
        this.force = force;
        this.puanteur = puanteur;
    }

    @Override
    public void attaque(Personnage p) {
        if (p instanceof Troll) {
            System.out.println("Un troll ne peut pas attaquer un autre troll.");
        } else {
            int degatsInfliges = force + puanteur / 10;
            p.setPointsVie(p.getPointsVie() - degatsInfliges);
            System.out.println(super.nom + " attaque " + p.nom +
                    " et lui inflige " + degatsInfliges + " points de dégâts.");
        }
    }


    @Override
    public int getPuanteur() {
        return puanteur;
    }

    @Override
    public String parler() {
        return "Grrr, je suis un troll.";
    }
}
