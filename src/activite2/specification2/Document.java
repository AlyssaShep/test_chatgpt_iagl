package activite2.specification2;

public class Document extends Objet {
    private int quantiteConnaissances;
    private boolean lu;

    public Document(String nom, int prix, int quantiteConnaissances) {
        super(nom, prix);
        this.quantiteConnaissances = quantiteConnaissances;
        this.lu = false;
    }

    public int getQuantiteConnaissances() {
        return quantiteConnaissances;
    }

    public boolean estLu() {
        return lu;
    }

    public void lire(Humanoid lecteur) {
        if (!lu) {
            lecteur.setConnaissances(lecteur.getConnaissances() + quantiteConnaissances);
            lu = true;
            System.out.println(lecteur.nom + " a lu le document '" + getNom() +
                    "' et a gagné " + quantiteConnaissances + " points de connaissances.");
        } else {
            System.out.println(lecteur.nom + " a déjà lu le document '" + getNom() + "'.");
        }
    }
}