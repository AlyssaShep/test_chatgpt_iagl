package activite2.specification2;

public class Elfe extends Humanoid {
    public Elfe(String nom, int x, int y, int v) {
        super(nom, x, y, v);
    }

    @Override
    public String parler() {
        return "Je suis un elfe.";
    }
}
