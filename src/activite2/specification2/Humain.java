package activite2.specification2;

// Classe représentant un personnage humanoïde spécifique (par exemple, un humain)
public class Humain extends Humanoid {
    public Humain(String nom, int x, int y, int v) {
        super(nom, x, y, v);
    }

    @Override
    public String parler() {
        return "Je suis un humain.";
    }
}
