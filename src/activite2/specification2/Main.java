package activite2.specification2;

public class Main {

    public static void main(String[] args) {

        // a)
        // Création d'objets
        Arme epee = new Arme("Épée elfique", 50, 10);
        Document parchemin = new Document("Parchemin ancien", 30, 15);
        AutreObjet bijou = new AutreObjet("Collier en argent", 10);
        // Création de personnages
        Elfe legolas = new Elfe("Legolas", 0, 0, 5);
        Humanoid aragorn = new Humain("Aragorn", 1, 1, 6);
        // Actions
        legolas.acquérirObjet(epee);
        aragorn.acquérirObjet(parchemin);
        legolas.seSéparerObjet(epee, aragorn);

        // b)
        // Actions
        legolas.acquérirObjet(epee);
        // Utilisation de l'arme
        legolas.utiliserArme("Épée elfique", aragorn);

        // c)
        Troll troll1 = new Troll("Grog", 5, 5, 8, 15, 20);
        Troll troll2 = new Troll("Grunt", 8, 8, 7, 12, 15);
        // Lecture du document
        aragorn.lireDocument(parchemin);
        aragorn.lireDocument(parchemin); // Essai de lire le même document à nouveau

        // Actions du troll
        System.out.println(troll1.parler());
        troll1.attaque(aragorn);
        troll2.attaque(troll1); // Le troll2 n'attaquera pas un autre troll

    }
}
