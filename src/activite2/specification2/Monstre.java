package activite2.specification2;

public interface Monstre {
    void attaque(Personnage p);
    int getPuanteur();
}
