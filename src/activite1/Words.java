package activite1;

public class Words implements Comparable<Words> {
    public String word;

    public int id;

    public Words(int id, String word){
        this.word = word;
        this.id = id;
    }

    @Override
    public int compareTo(Words autre) {
        return this.word.compareTo(autre.word);
    }

}
