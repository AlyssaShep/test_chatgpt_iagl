package activite1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ComposedWords {
    private TrieNode trie = new TrieNode();

    public TrieNode getComposedWordsTrie() {
        return trie;
    }

    // Méthode pour vérifier si un terme est un mot composé et obtenir son ID.
    public int getComposedWordId(String term) {
        TrieNode current = trie;
        for (char c : term.toCharArray()) {
            current = current.children.get(c);
            if (current == null) {
                return -1;  // Le terme n'est pas un mot composé.
            }
        }
        return current.id;
    }

    // Méthode pour trouver les mots composés avec un préfixe donné.
    public List<Pair<String, Integer>> findComposedWordsWithPrefix(String prefix) {
        TrieNode current = trie;
        for (char c : prefix.toCharArray()) {
            current = current.children.get(c);
            if (current == null) {
                return null;  // Aucun mot composé avec ce préfixe.
            }
        }

        // Utilisation de DFS pour récupérer les mots composés avec le préfixe.
        List<Pair<String, Integer>> result = new ArrayList<>();
        findWordsWithPrefixDFS(current, prefix, result);
        return result;
    }

    private void findWordsWithPrefixDFS(TrieNode node, String currentPrefix, List<Pair<String, Integer>> result) {
        if (node.id != -1) {
            result.add(new Pair<>(currentPrefix, node.id));
        }

        for (Map.Entry<Character, TrieNode> entry : node.children.entrySet()) {
            findWordsWithPrefixDFS(entry.getValue(), currentPrefix + entry.getKey(), result);
        }
    }

    // Classe pour représenter une paire (mot composé, id).
    public static class Pair<T, U> {
        T first;
        U second;

        Pair(T first, U second) {
            this.first = first;
            this.second = second;
        }
    }
}

