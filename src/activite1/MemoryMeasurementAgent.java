package activite1;

import java.lang.instrument.Instrumentation;

public class MemoryMeasurementAgent {
    private static Instrumentation instrumentation;

    public static void premain(String agentArgs, Instrumentation inst) {
        instrumentation = inst;
    }

    public static long getObjectSize(Object obj) {
        if (instrumentation == null) {
            throw new IllegalStateException("L'agent d'instrumentation n'a pas été initialisé.");
        }
        return instrumentation.getObjectSize(obj);
    }
}
