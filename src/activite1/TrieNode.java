package activite1;

import java.util.HashMap;
import java.util.Map;

public class TrieNode {
    Map<Character, TrieNode> children = new HashMap<>();
    int id = -1;  // L'id du mot composé, -1 s'il n'y en a pas.

    // Constructeur par défaut.
    TrieNode() {}

    // Méthode pour ajouter un mot composé au trie.
    void addWord(String word, int id) {
        TrieNode current = this;
        for (char c : word.toCharArray()) {
            current.children.putIfAbsent(c, new TrieNode());
            current = current.children.get(c);
        }
        current.id = id;
    }

}