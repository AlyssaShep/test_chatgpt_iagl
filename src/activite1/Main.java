package activite1;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Main {

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    public static long getMemoryUsage() {
        // Créez un objet factice pour mesurer son empreinte mémoire
        List<String> dummyList = new ArrayList<>();
        MemoryMeasurementAgent.getObjectSize(dummyList); // Permet d'initialiser l'instrumentation

        // Mesure de l'empreinte mémoire de l'objet factice
        return MemoryMeasurementAgent.getObjectSize(dummyList);
    }

    public static void main(String[] args) {
        // Création de l'objet ComposedWords
//        ComposedWords composedWords = new ComposedWords();

        // Mesure de l'empreinte mémoire
//        long memoryBefore = getMemoryUsage();

        Dico dico = new Dico();

        // Ajout des termes composés à partir de votre fichier F.txt

        String chemin = "20231207-LEXICALNET-JEUXDEMOTS-ENTRIES-MWE.txt";

        try (BufferedReader reader = Files.newBufferedReader(Paths.get(chemin))) {
            String line;
            while ((line = reader.readLine()) != null) {
                // Ignorer les lignes de commentaire
                if (line.startsWith("//") || line.trim().isEmpty()) {
                    continue;
                }

                // Diviser la ligne en utilisant le point-virgule comme séparateur
                String[] parts = line.split(";", 3);

                // Vérifier si la ligne a le format attendu
                if (parts.length >= 2) {
                    int id = Integer.parseInt(parts[0].replaceAll("\"", "").trim());
                    String term = parts[1].replaceAll("\"", "").trim();

                    // Ajouter chaque terme composé avec son ID
//                    composedWords.getComposedWordsTrie().addWord(term, id);
                    dico.add(id, term);

                } else {
                    System.err.println("Format de ligne incorrect : " + line);
                }
            }
        } catch (IOException e) {
            System.err.println("Erreur lors de la lecture du fichier : " + e.getMessage());
        }

        dico.tri();

        // Exemple (à remplacer par la lecture réelle du fichier) :
//        composedWords.getComposedWordsTrie().addWord("chat de gouttière", 1);
//        composedWords.getComposedWordsTrie().addWord("organisme vivant", 2);
//        // ... Ajouter d'autres termes
//
//        // Exécution des tests
        Scanner scanner = new Scanner(System.in);
//
//        // Test 1: Vérifier si un terme est un mot composé et obtenir son ID.
        System.out.print("Entrez un terme pour obtenir son ID : ");
        String term = scanner.nextLine();

        long startTime = System.currentTimeMillis();

        int id = dico.getIdFromWord(term);
//        int id = composedWords.getComposedWordId(term);
        if (id != -1) {
            System.out.println("Le terme est un mot composé avec l'ID : " + id);
        } else {
            System.out.println("Le terme n'est pas un mot composé.");
        }

        long endTime = System.currentTimeMillis();
        // Calcul de la durée en millisecondes
        long duration = endTime - startTime;
        System.out.println("Temps d'exécution du premier bloc de code : " + duration + " millisecondes");

//
//        // Test 2: Trouver les mots composés avec un préfixe donné.
        System.out.print("Entrez un préfixe pour trouver les mots composés : ");
        String prefix = scanner.nextLine();
        startTime = System.currentTimeMillis();
        ArrayList<Words> prefixes = new ArrayList<>();
        dico.getPrefixe(prefix, prefixes);

        if (prefixes.isEmpty()){
            System.out.println("Aucun mot composé trouvé avec ce préfixe.");
        } else {
            System.out.println("Mots composés avec le préfixe :");
            for (Words w : prefixes){
                System.out.println("Mot : " + w.word +" - Id : " + w.id);
            }
        }
//        List<ComposedWords.Pair<String, Integer>> result = composedWords.findComposedWordsWithPrefix(prefix);
//        if (result != null && !result.isEmpty()) {
//            System.out.println("Mots composés avec le préfixe :");
//            for (ComposedWords.Pair<String, Integer> pair : result) {
//                System.out.println(pair.first + " (ID : " + pair.second + ")");
//            }
//        } else {
//            System.out.println("Aucun mot composé trouvé avec ce préfixe.");
//        }

        endTime = System.currentTimeMillis();
        // Calcul de la durée en millisecondes
        duration = endTime - startTime;
        System.out.println("Temps d'exécution du deuxieme bloc de code : " + duration + " millisecondes");

//        // Mesure de l'empreinte mémoire après l'exécution de votre code
//        long memoryAfter = getMemoryUsage();
//
//        // Calcul de la différence
//        long memoryUsage = memoryAfter - memoryBefore;
//
//        System.out.println("Empreinte mémoire : " + memoryUsage + " octets");

        // Get the Java runtime
        System.out.println();
        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
//        System.out.println("Used memory is bytes: " + memory);
        System.out.println("Empreinte mémoire en octets: " + memory);
        System.out.println("Empreinte mémoire en Mo: " + bytesToMegabytes(memory));

    }

}
