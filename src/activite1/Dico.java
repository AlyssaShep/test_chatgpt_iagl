package activite1;

import java.util.ArrayList;
import java.util.Collections;

public class Dico {

    public ArrayList<Words> dico = new ArrayList<>();

    public void add(int id, String w){
        dico.add(new Words(id, w));
    }

    public void tri(){
        Collections.sort(dico);
    }

    public int getIdFromWord(String term){

        int size = dico.size();

        for(int i = 0; i < size; i++){
            if (dico.get(i).word.equals(term)){
                return dico.get(i).id;
            }
        }

        return -1;
    }

    public void getPrefixe(String pre, ArrayList<Words> prefixes){
        for (Words words : dico) {
            if (words.word.startsWith(pre)) {
                prefixes.add(words);
            }
        }
    }
}
