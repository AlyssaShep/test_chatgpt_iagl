package activite3;

public class Noeud {
    int valeur;
    Noeud gauche;
    Noeud droite;
    int hauteur;

    Noeud(int valeur) {
        this.valeur = valeur;
        this.gauche = null;
        this.droite = null;
        this.hauteur = 1;
    }
}