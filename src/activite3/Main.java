package activite3;

public class Main {

    public static void main(String[] args) {
        // Exemple d'utilisation de l'arbre AVL
        Noeud racine = null;
        racine = AVLTree.inserer(racine, 10);
        racine = AVLTree.inserer(racine, 20);
        racine = AVLTree.inserer(racine, 3);
        racine = AVLTree.inserer(racine, 40);
        racine = AVLTree.inserer(racine, 50);

        System.out.println("Affichage de l'arbre AVL après insertion:");
        AVLTree.afficherArbre(racine, 0);

        racine = AVLTree.supprimer(racine, 30);

        System.out.println("\nAffichage de l'arbre AVL après suppression:");
        AVLTree.afficherArbre(racine, 0);

        // Libérer la mémoire
        AVLTree.libererMemoire(racine);
    }

}
