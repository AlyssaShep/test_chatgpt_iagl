#include <stdio.h>
#include <stdlib.h>

// Structure d'un noeud de l'AVL
typedef struct Noeud
{
    int valeur;
    struct Noeud *gauche;
    struct Noeud *droite;
    int hauteur;
} Noeud;

// Fonction pour créer un nouveau noeud
Noeud *creerNoeud(int valeur);

// Fonction pour calculer la hauteur d'un noeud
int hauteur(Noeud *N);

// Fonction pour calculer le maximum de deux entiers
int max(int a, int b);

// Fonction pour effectuer une rotation à droite simple
Noeud *faireSimpleRotationADroite(Noeud *y);

// Fonction pour effectuer une rotation à gauche simple
Noeud *faireSimpleRotationAGauche(Noeud *x);

// Fonction pour effectuer une rotation à droite double
Noeud *faireDoubleRotationADroite(Noeud *z);

// Fonction pour effectuer une rotation à gauche double
Noeud *faireDoubleRotationAGauche(Noeud *z);

// Fonction pour équilibrer un arbre AVL après une insertion
Noeud *equilibrerArbre(Noeud *racine, int valeur);

// Fonction pour insérer une valeur dans l'arbre AVL
Noeud *inserer(Noeud *racine, int valeur);

// Fonction pour trouver le successeur d'un noeud
Noeud *trouverSuccesseur(Noeud *racine);

// Fonction pour supprimer un noeud de l'arbre AVL
Noeud *supprimer(Noeud *racine, int valeur);

// Fonction pour afficher l'arbre AVL de manière récursive
void afficherArbre(Noeud *racine, int espace);

// Fonction pour libérer la mémoire utilisée par l'arbre AVL
void libererMemoire(Noeud *racine);
