#include <stdio.h>
#include <stdlib.h>

// Macros pour les opérations répétitives
#define MAX(a, b) ((a > b) ? a : b)
#define HAUTEUR(N) ((N == NULL) ? 0 : N->hauteur)

// Structure d'un noeud de l'AVL
typedef struct Noeud
{
    int valeur;
    struct Noeud *gauche;
    struct Noeud *droite;
    int hauteur;
} Noeud;

// Macro pour créer un nouveau noeud
#define CREER_NOEUD(valeur) creerNoeud(valeur)

// Macro pour libérer la mémoire d'un noeud
#define LIBERER_MEMOIRE(racine) libererMemoire(racine)

// Fonction pour créer un nouveau noeud
Noeud *creerNoeud(int valeur)
{
    Noeud *nouveauNoeud = (Noeud *)malloc(sizeof(Noeud));
    if (nouveauNoeud == NULL)
    {
        fprintf(stderr, "Erreur d'allocation de mémoire\n");
        exit(EXIT_FAILURE);
    }
    nouveauNoeud->valeur = valeur;
    nouveauNoeud->gauche = NULL;
    nouveauNoeud->droite = NULL;
    nouveauNoeud->hauteur = 1;
    return nouveauNoeud;
}

// Fonction pour libérer la mémoire utilisée par l'arbre AVL
void libererMemoire(Noeud *racine)
{
    if (racine == NULL)
        return;
    libererMemoire(racine->gauche);
    libererMemoire(racine->droite);
    free(racine);
}

// Macros pour le code du main et les tests
#define TEST_AVL_OPERATIONS() \
    testInserer();             \
    testSupprimer();           \
    testAfficherArbre();

// Fonction pour afficher l'arbre AVL de manière récursive
void afficherArbre(Noeud *racine, int espace)
{
    if (racine == NULL)
        return;
    espace += 10;
    afficherArbre(racine->droite, espace);
    printf("\n");
    for (int i = 10; i < espace; i++)
        printf(" ");
    printf("%d\n", racine->valeur);
    afficherArbre(racine->gauche, espace);
}

// Fonction pour effectuer une rotation à droite simple
Noeud *faireSimpleRotationADroite(Noeud *y)
{
    Noeud *x = y->gauche;
    Noeud *T2 = x->droite;
    x->droite = y;
    y->gauche = T2;
    y->hauteur = MAX(HAUTEUR(y->gauche), HAUTEUR(y->droite)) + 1;
    x->hauteur = MAX(HAUTEUR(x->gauche), HAUTEUR(x->droite)) + 1;
    return x;
}

// Fonction pour effectuer une rotation à gauche simple
Noeud *faireSimpleRotationAGauche(Noeud *x)
{
    Noeud *y = x->droite;
    Noeud *T2 = y->gauche;
    y->gauche = x;
    x->droite = T2;
    x->hauteur = MAX(HAUTEUR(x->gauche), HAUTEUR(x->droite)) + 1;
    y->hauteur = MAX(HAUTEUR(y->gauche), HAUTEUR(y->droite)) + 1;
    return y;
}

// Fonction pour effectuer une rotation à droite double
Noeud *faireDoubleRotationADroite(Noeud *z)
{
    z->gauche = faireSimpleRotationAGauche(z->gauche);
    return faireSimpleRotationADroite(z);
}

// Fonction pour effectuer une rotation à gauche double
Noeud *faireDoubleRotationAGauche(Noeud *z)
{
    z->droite = faireSimpleRotationADroite(z->droite);
    return faireSimpleRotationAGauche(z);
}

// Fonction pour équilibrer un arbre AVL après une insertion
Noeud *equilibrerArbre(Noeud *racine, int valeur)
{
    int equilibre = HAUTEUR(racine->gauche) - HAUTEUR(racine->droite);
    if (equilibre > 1 && valeur < racine->gauche->valeur)
        return faireSimpleRotationADroite(racine);
    if (equilibre < -1 && valeur > racine->droite->valeur)
        return faireSimpleRotationAGauche(racine);
    if (equilibre > 1 && valeur > racine->gauche->valeur)
    {
        racine->gauche = faireSimpleRotationAGauche(racine->gauche);
        return faireSimpleRotationADroite(racine);
    }
    if (equilibre < -1 && valeur < racine->droite->valeur)
    {
        racine->droite = faireSimpleRotationADroite(racine->droite);
        return faireSimpleRotationAGauche(racine);
    }
    return racine;
}

// Fonction pour insérer une valeur dans l'arbre AVL
Noeud *inserer(Noeud *racine, int valeur)
{
    if (racine == NULL)
        return CREER_NOEUD(valeur);
    if (valeur < racine->valeur)
        racine->gauche = inserer(racine->gauche, valeur);
    else if (valeur > racine->valeur)
        racine->droite = inserer(racine->droite, valeur);
    else
        return racine;
    racine->hauteur = MAX(HAUTEUR(racine->gauche), HAUTEUR(racine->droite)) + 1;
    return equilibrerArbre(racine, valeur);
}

// Fonction pour trouver le successeur d'un noeud
Noeud *trouverSuccesseur(Noeud *racine)
{
    Noeud *courant = racine;
    while (courant->gauche != NULL)
        courant = courant->gauche;
    return courant;
}

// Fonction pour supprimer un noeud de l'arbre AVL
Noeud *supprimer(Noeud *racine, int valeur)
{
    if (racine == NULL)
        return racine;
    if (valeur < racine->valeur)
        racine->gauche = supprimer(racine->gauche, valeur);
    else if (valeur > racine->valeur)
        racine->droite = supprimer(racine->droite, valeur);
    else
    {
        if (racine->gauche == NULL || racine->droite == NULL)
        {
            Noeud *temp = (racine->gauche) ? racine->gauche : racine->droite;
            if (temp == NULL)
            {
                temp = racine;
                racine = NULL;
            }
            else
                *racine = *temp;
            free(temp);
        }
        else
        {
            Noeud *temp = trouverSuccesseur(racine->droite);
            racine->valeur = temp->valeur;
            racine->droite = supprimer(racine->droite, temp->valeur);
        }
    }
    if (racine == NULL)
        return racine;
    racine->hauteur = MAX(HAUTEUR(racine->gauche), HAUTEUR(racine->droite)) + 1;
    return equilibrerArbre(racine, valeur);
}

// Fonction de test d'insertion
void testInserer()
{
    Noeud *racine = NULL;
    racine = inserer(racine, 10);
    racine = inserer(racine, 20);
    racine = inserer(racine, 30);
    racine = inserer(racine, 40);
    racine = inserer(racine, 50);

    printf("Arbre AVL après insertion :\n");
    afficherArbre(racine, 0);
}

// Fonction de test de suppression
void testSupprimer()
{
    Noeud *racine = NULL;
    racine = inserer(racine, 9);
    racine = inserer(racine, 5);
    racine = inserer(racine, 10);
    racine = inserer(racine, 0);
    racine = inserer(racine, 6);
    racine = inserer(racine, 11);
    racine = inserer(racine, -1);
    racine = inserer(racine, 1);
    racine = inserer(racine, 2);

    printf("Arbre AVL avant suppression :\n");
    afficherArbre(racine, 0);

    racine = supprimer(racine, 10);

    printf("Arbre AVL après suppression de 10 :\n");
    afficherArbre(racine, 0);
}

// Fonction de test pour afficher l'arbre AVL
void testAfficherArbre()
{
    Noeud *racine = NULL;
    racine = inserer(racine, 9);
    racine = inserer(racine, 5);
    racine = inserer(racine, 10);
    racine = inserer(racine, 0);
    racine = inserer(racine, 6);
    racine = inserer(racine, 11);
    racine = inserer(racine, -1);
    racine = inserer(racine, 1);
    racine = inserer(racine, 2);

    printf("Arbre AVL :\n");
    afficherArbre(racine, 0);
}

// Macro pour générer le code du main
#define GENERER_MAIN() \
    int main()          \
    {                   \
        TEST_AVL_OPERATIONS(); \
        return 0;       \
    }

// Code du main généré
GENERER_MAIN()
